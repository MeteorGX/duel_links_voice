extends Control

# 替换语音的按钮
export(NodePath) var replace_button

# 替换按钮
func replace_button_status(disable:bool):
	if self.replace_button != null:
		var node = get_node(self.replace_button)
		if node != null:
			node.disabled = disable
