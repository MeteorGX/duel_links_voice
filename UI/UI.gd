extends Control

###
###
### 	Duel Link 国际服语音替换时候的具体流程( 从刚开始安装游戏 )
###		@Author: MeteorCat
###		@Date: 2022-01-14
###
###		1. 首先刚从游戏安装DuelLink不会在游戏目录生成 LocalData 目录, 这个目录用于放置卡图和语音文件
###		2. 当首次启动之后游戏会重启, 这时候就已经生成 LocalData 目录
###		3. 生成目录的时候是可以判断 LocalData/xxxxxxxx/e2/e2264531 文件是否存在, 如果不存在代表游戏并没有安装韩/英语音; 这时候不要替换, 必须等其他语音包安装完整
###			3.1 如果有多个 DuelLink 游戏账号登录过的话, LocalData 目录下面有大量子目录
###			3.2 出现多个账号的情况, 这里采用让其手动清理 LocalData 内部多余的子目录
###		4. 最后检测 LocalData/xxxxxxxx/e2/e2264531 目录是否存在, 如果存在则显示已存在语音文件, 且可以重新删除替换原文件; 如果不存在, 则直接显示可以替换直接复制数据过去
###
###


# 决斗链接的游戏 APPID
const steam_app_id:int = 601510

# Steam 游戏的目录
export(String) var steam_app_path = ""

# 决斗链接挂载的资源文件
onready var steam_voice_filename = "res://Files/e2264531.res"

# 决斗链接文件语音替换文件
var steam_voice_file:File = File.new()

# 加载目录句柄
var directory_handler:Directory = Directory.new()

# 确认决斗链接语音卡图等资源文件夹名
var duel_link_resource_dirname:String = "LocalData"

# 确认决斗链接语音文件名称判定
var duel_link_voice_name = "e2264531"

# 决斗链接内部的存档列表
var duel_link_directory_list = []

# 决斗链接的语音文件放置目录
var duel_link_resource_voice:String = "e2"

# 最后完美获取的决斗链接替换语音文件
var duel_link_resource_voice_filename:String = ""

# 确认内部含有语音包文件
var duel_link_voice_exists:String = "e2370bf8"


# 启动时获取目前的游戏目录
func _ready():
	
	# 获取并判断游戏是否存在
	self.steam_app_path = SteamHelper.search_steam_app_path_by_64(self.steam_app_id)
	if self.steam_app_path.empty():
		Logger.error("找不到游戏[ {app_id} ]".format({"app_id":self.steam_app_id}))
		get_tree().quit(1)
		return 
	else:
		Logger.info("找到游戏[ {app_id} ] = {path}".format({"app_id":self.steam_app_id,"path":self.steam_app_path}))
		
	# 判断语音文件是否存在
	if not self.directory_handler.file_exists(self.steam_voice_filename):
		Logger.error("找不到替换的语音文件[ {filename} ]".format({"filename":self.steam_voice_filename}))
		get_tree().quit(1)
		return 
	
	
	# 确保游戏安装之后已经启动
	var dl_local_path = "{path}\\{local}".format({"path":self.steam_app_path,"local":self.duel_link_resource_dirname})
	if not self.directory_handler.dir_exists(dl_local_path):
		$Body.voice_status_message("暂时无法替换语音包",true)
		$Body.replace_status_message("请先启动游戏后关闭游戏再运行",true)
		$Footer.replace_button_status(true)
		Logger.error("游戏首次启动依赖未安装")
		return 
		
		
	# 记录目前的获取到的日志
	Logger.info("游戏资源文件夹已存在[ {local} ]".format({"local":dl_local_path}))
	
	# 确认内部文件是否有多个 duel links 存档
	if self.directory_handler.open(dl_local_path) != OK:
		$Body.voice_status_message("暂时无法替换语音包",true)
		$Body.replace_status_message("无法打开游戏目录",true)
		$Footer.replace_button_status(true)
		Logger.error("无法打开游戏目录")
		return 
		
	# 获取内部所有的记录目录
	if self.directory_handler.list_dir_begin(true) != OK:
		$Body.voice_status_message("暂时无法替换语音包",true)
		$Body.replace_status_message("无法获取内部文件列表",true)
		$Footer.replace_button_status(true)
		Logger.error("无法获取内部文件列表")
		return 
	
	
	# 遍历内部所有的子目录
	var file_name = self.directory_handler.get_next()
	while file_name != "":
		self.duel_link_directory_list.append(file_name)
		file_name = self.directory_handler.get_next()
		
	
	# 确认是否找不到内部任何子目录
	if self.duel_link_directory_list.empty():
		$Body.voice_status_message("暂时无法替换语音包",true)
		$Body.replace_status_message("找不到游戏记录",true)
		$Footer.replace_button_status(true)
		Logger.error("找不到游戏记录")
		return 
	
	
	# 内部曾经含有多个存档
	if self.duel_link_directory_list.size() > 1:
		$Body.voice_status_message("暂时无法替换语音包",true)
		$Body.replace_status_message("{dir} 目录含有大量存档".format({"dir":self.duel_link_resource_dirname}),true)
		$Footer.replace_button_status(true)
		Logger.error("内部曾经还有大量存档 = {list}".format({"list":self.duel_link_directory_list}))
		return 
		
		
	# 获取最后的目录
	var voice_path = self.duel_link_directory_list.pop_back()
	if voice_path == null:
		$Body.voice_status_message("暂时无法替换语音包",true)
		$Body.replace_status_message("找不到资源目录",true)
		$Footer.replace_button_status(true)
		Logger.error("找不到资源目录")
		return 

	
	# 确认最后的目录是否存在
	var e2_dir = "{local}\\{resource}\\{e2}".format({"local":dl_local_path,"resource":voice_path,"e2":self.duel_link_resource_voice})
	if not self.directory_handler.dir_exists(e2_dir):
		$Body.voice_status_message("暂时无法替换语音包",true)
		$Body.replace_status_message("找不到资源目录",true)
		$Footer.replace_button_status(true)
		Logger.error("找不到资源目录")
		return 
	Logger.info("确认已经找到资源目录 = {dir}".format({"dir":e2_dir}))

	# 确认配置文件存在
	self.duel_link_resource_voice_filename = "{e2}\\{file}".format({"e2":e2_dir,"file":self.duel_link_voice_name})
	if not self.directory_handler.file_exists(self.duel_link_resource_voice_filename):
		$Body.voice_status_message("暂时无法替换语音包",true)
		$Body.replace_status_message("无法找到语音配置文件",true)
		$Footer.replace_button_status(true)
		Logger.error("无法找到语音配置文件 = {file}".format({"file":self.directory_handler}))
		return
	
	# 确认语音包安装
	var voice_exists = self.directory_handler.file_exists("{e2}\\{file}".format({"e2":e2_dir,"file":self.duel_link_voice_exists}))
	if not voice_exists:
		$Body.voice_status_message("暂时无法替换语音包",true)
		$Body.replace_status_message("请先下载英/韩语音包",true)
		$Footer.replace_button_status(true)
		Logger.error("找不到资源目录")
		return 
	
	# 开放替换语音包功能
	$Body.voice_status_message("可以进行语音替换",false)
	$Body.replace_status_message("语音替换功能正常",false)
	$Footer.replace_button_status(false)


# 退出清理资源
func _exit_tree():
	Logger.info("准备退出清理: 已关闭语音文件句柄")
	self.steam_voice_file.close()


# 点击时候退出应用
func _on_CloseButton_button_up():
	Logger.info("退出软件")
	get_tree().quit()
	return 

# 点击打开目录
func _on_OpenButton_button_up():
	var open_status = OS.shell_open(self.steam_app_path)
	Logger.info("打开游戏目录 = {path} | 返回状态 = {status}".format({"path":self.steam_app_path,"status":open_status}))


# 替换日语语音文件
func _on_ReplaceButton_button_up():
	var filename = self.duel_link_resource_voice_filename
	
	# 如果存在则删除
	if self.directory_handler.file_exists(filename):
		
		# 确认源配置文件被删除
		if self.directory_handler.remove(filename) != OK:
			Logger.error("无法打开指定配置文件 = {file}".format({"file":filename}))
			$AlertBox.show_alert("请确认游戏关闭且没有被占用","替换失败")
			return
	
	# 判断初始化关闭
	self.steam_voice_file.close()
	
	
	# 打开最开始的日语配置文件
	if self.steam_voice_file.open(self.steam_voice_filename,File.READ) != OK:
		Logger.error("无法读取指定的日语配置文件 = {file}".format({"file":self.steam_voice_filename}))
		$AlertBox.show_alert("无法写入配置文件, 请确认游戏关闭和文件不被占用","替换失败")
		return
	
	
	# 读取内部的配置数据
	var file_bytes = self.steam_voice_file.get_buffer(self.steam_voice_file.get_len())
	self.steam_voice_file.close()
	
	
	# 初始化文件句柄
	if self.steam_voice_file.open(filename, File.WRITE) != OK:
		Logger.error("无法打开指定配置文件 = {file}".format({"file":filename}))
		$AlertBox.show_alert("无法写入配置文件, 请确认游戏关闭和文件不被占用","替换失败")
		return

	# 写入内部配置
	self.steam_voice_file.store_buffer(file_bytes)
	self.steam_voice_file.close()
	
	# 写入入职和弹出提示
	Logger.info("执行日语配置文件替换 = {file}".format({"file":filename}))
	$AlertBox.show_alert("请进入游戏标题点击 '已重新下载游戏数据'\r\n期间会进行多次重启,如果过多次请重启电脑进入游戏","替换成功")
