extends AcceptDialog

# 展示提示的指定窗口内容
func show_alert(message:String,title:String):
	self.dialog_text = message
	self.window_title = title
	self.popup()
