extends Control

# 日语语音是否存在的标签节点
export(NodePath) var voice_status

# 资源文件夹是否存在的标签节点
export(NodePath) var replace_status


# 替换语音是否存在的节点
func voice_status_message(message:String, err:bool = false):
	if self.voice_status != null:
		var node = get_node(self.voice_status)
		if node != null:
			node.text = message
			if err:
				node.add_color_override("font_color",Color.yellow)
			else:
				node.add_color_override("font_color",Color.webgreen)


# 替换资源文件夹是否存在的节点
func replace_status_message(message:String, err:bool = false):
	if self.replace_status != null:
		var node = get_node(self.replace_status)
		if node != null:
			node.text = message
			if err:
				node.add_color_override("font_color",Color.yellow)
			else:
				node.add_color_override("font_color",Color.webgreen)
