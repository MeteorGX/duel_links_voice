extends Node

# 默认的格式化日志方法
const DEFAULT_LOGGER_FORMAT = "[{level}] ({datetime}): {message}"

# 默认的保存日志文件
const DEFAULT_LOGGER_FILE_NAME = "dl_replace_{date}.log"

# 环境枚举
enum Env {
	Development,
	Production
}

# 定义目前的日志环境
export(Env) var logger_env = Env.Production

# 定义默认的写入文件句柄
var logger_file_handler:File = File.new()


# 退出游戏的时候清理文件句柄
func _exit_tree():
	self.logger_file_handler.flush()
	self.logger_file_handler.close()
	

# 判断目前是否为正式环境
func is_release()->bool:
	return OS.has_feature("release")
	
# 判断目前是否为调试环境
func is_debug()->bool:
	return OS.has_feature("debug")

# 写入日志
func write_logger(message:String, level:String, format:String):
	var datetime = Datetime.date();
	
	var result:String = format.format({"level":level,"datetime":datetime,"message":message})
	if is_debug() and self.logger_env != Env.Production:
		print(result)
	else:
		# 确认写入的日志文件
		var log_filename = DEFAULT_LOGGER_FILE_NAME.format({"date":Datetime.date("{yyyy}_{MM}_{dd}")})
		
		# 确认文件打开, 并可以写入
		if not self.logger_file_handler.is_open():
			if self.logger_file_handler.open(log_filename,File.WRITE_READ) != OK:
				get_tree().quit(1)
				return
				
			
		# 写入文件日志
		self.logger_file_handler.store_line(result)
		self.logger_file_handler.flush()
		return 
	

# 写入 INFO 类型日志
func info(message:String, format:String = DEFAULT_LOGGER_FORMAT):
	return self.write_logger(message,"INFO ", format)

# 写入 ERROR 类型日志
func error(message:String, format:String = DEFAULT_LOGGER_FORMAT):
	return self.write_logger(message,"ERROR", format)

# 写入 DEBUG 类型日志
func debug(message:String, format:String = DEFAULT_LOGGER_FORMAT):
	return self.write_logger(message,"DEBUG", format)
