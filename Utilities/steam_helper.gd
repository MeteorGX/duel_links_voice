extends Node

# 64 位环境检查 Steam 应用
const ENV_VAR_BY_64:String = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Steam App {app_id}"

# 32 位环境检测 Steam 应用
const ENV_VAR_BY_32:String = "HKEY_LOCAL_MACHINE\\SOFTWARE\\WOW6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Steam App {app_id}"

# 检查 Steam 的游戏内部路径( 64位系统用 )
func search_steam_app_path_by_64(app_id:int)->String:
	var cmd :String = ENV_VAR_BY_64.format({'app_id':app_id})
	return self.execute_search_steam_command(cmd)
	
# 检查 Steam 的游戏内部路径( 32位系统用 )
func search_steam_app_path_by_32(app_id:int)->String:
	var cmd :String = ENV_VAR_BY_32.format({'app_id':app_id})
	return self.execute_search_steam_command(cmd)
	
# 执行 Steam 游戏查找命令
func execute_search_steam_command(cmd:String)->String:
	var output = []
	var ec = OS.execute("CMD.exe",["/C","reg","query",cmd], true, output)
	
	# 返回错误码, 表明执行错误, 没找到 Steam 客户端变量 ？
	if ec != 0:
		return ""
		
	# 检索截取返回的输出数据
	for key in output:
		# 截取所有的换行符
		var lines = key.split("\n")
		
		# 获取所有的行信息
		for line in lines:
			# 确认行数据正确
			if not line.empty() and typeof(line) == TYPE_STRING:
				var line_string: String = line
				# 确认内部含有路径关键字
				if line_string.find_last("InstallLocation") >= 0:
					# 整理好需要配置数据
					line_string = line_string.replace("InstallLocation","")
					line_string = line_string.replace("REG_SZ","")
					line_string = line_string.strip_edges()
					
					# 确认好路径在本地电脑存在
					if not line_string.empty():
						var dir:Directory = Directory.new()
						if dir.dir_exists(line_string):
							# 存在该路径则返回
							return line_string
	return ""
	

