extends Node

# 默认的时间格式
const DEFAULT_DATE_FORMAT:String = "{yyyy}-{MM}-{dd} {HH}:{mm}:{ss}"

# 获取秒级别时间戳
func time()->int:
	return OS.get_unix_time()

# 获取日期时间
func date( format:String = DEFAULT_DATE_FORMAT, time:int = 0, is_utc:bool = false)->String:
	var t = null
	if time == 0:
		t = OS.get_datetime(is_utc)
	else:
		t = OS.get_datetime_from_unix_time(time)
	
	if t == null:
		return "Unkown"
	
	var result = format.format({"MM": str(t.month).pad_zeros(2), "dd": str(t.day).pad_zeros(2), "yyyy": str(t.year).pad_zeros(4), "HH": str(t.hour).pad_zeros(2), "mm": str(t.minute).pad_zeros(2), "ss": str(t.second).pad_zeros(2)})
	return result
